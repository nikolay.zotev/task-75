import "./App.css";

import { useEffect, useState } from "react";

function App() {
  const [note, setNote] = useState("");

  const getFromLocalStorage = () => {
    const noteData = localStorage.getItem("note-data");

    if (noteData) {
      return JSON.parse(noteData).text;
    }

    return null;
  };

  const saveToLocalStorage = () => {
    localStorage.setItem(
      "note-data",
      JSON.stringify({
        text: note,
      })
    );
  };

  const clearLocalStorage = () => {
    setNote("");
    localStorage.clear("note-data");
  };

  const onChange = (e) => {
    setNote(e.target.value);
  };

  useEffect(() => {
    const note = getFromLocalStorage();

    if (note) {
      setNote(note);
    }
  }, []);

  return (
    <div className="App">
      <div className="box">
        <div className="field">
          <div className="control">
            <textarea
              className="textarea is-large"
              placeholder="Notes..."
              value={note}
              onChange={onChange}
            />
          </div>
        </div>
        <button
          className="button is-large is-primary is-active"
          onClick={saveToLocalStorage}
        >
          Save
        </button>
        <button className="button is-large" onClick={clearLocalStorage}>
          Clear
        </button>
      </div>
    </div>
  );
}

export default App;
